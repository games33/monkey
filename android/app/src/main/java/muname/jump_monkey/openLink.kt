package com.cosmo.asteroid_crasher

import android.content.Context
import android.content.Intent
import android.net.Uri

fun Context.openLink(link: String?) {
    link?.let {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it)).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }

        startActivity(browserIntent)
    }
}