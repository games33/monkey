package muname.jump_monkey;

import io.flutter.embedding.android.FlutterActivity;

public class MainActivity extends FlutterActivity {
    @Override protected void onResume() {
        super.onResume();
        App.Companion.checkLink(this);
    }
}
