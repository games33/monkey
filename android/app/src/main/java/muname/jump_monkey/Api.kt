package muname.jump_monkey

import androidx.annotation.Keep
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.util.*


//"https://getldn.xyz/b.php"


object ApiService {
    private val api get() = Firebase.remoteConfig.getString("api")

    var client = OkHttpClient()

    @Throws(IOException::class)
    fun load(): Response? {
        val header = System.getProperty("http.agent")
        val request: Request = Request.Builder()
            .header("Accept-Language", Locale.getDefault().toLanguageTag())
            .header("user-Agent", header ?: "Android")
            .url(api)
            .build()
        return client.newCall(request).execute().body
            ?.string()?.fromJson<Response>()
    }


    private inline fun <reified T> String.fromJson() =
        Gson().fromJson<T>(this, object : TypeToken<T>() {}.type)

}

@Keep
data class Response(val proper: Int, val url: String?)
