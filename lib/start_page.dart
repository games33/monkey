import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> with TickerProviderStateMixin {
  AnimationController sliderController;
  AnimationController monkeyController;
  double positionBanana = 120;
  // double jump = 80;
  Animation<double> monkeyAnimation;
  Animation<double> sliderAnimation;
  Tween<double> monkeyTween;
  Tween<double> sliderTween;
  int sliderSpeed = 10000;
  int jumpMonkeySpeed = 200;
  bool visibleBanana = true;
  bool visibleText = false;
  int level = 0;
  int highJump = 10;
  int refreshPositionBanana = 0;
  int refreshLevel = 0;

  void jumpMonkey() {
    if (sliderAnimation.value >= 140 && sliderAnimation.value <= 180) {
      monkeyController.forward();
      monkeyTween.end = 90;
      highJump += 20;
      refreshPositionBanana = 20;
      ++refreshLevel;
      monkeyTween.end += highJump;
      visibleBanana = false;
      switch (sliderSpeed) {
        case 10000:
          sliderSpeed = 8000;
          break;
        case 8000:
          sliderSpeed = 6000;
          break;
        case 6000:
          sliderSpeed = 4000;
          break;
        case 4000:
          sliderSpeed = 3000;
          break;
        case 3000:
          sliderSpeed = 2000;
          break;
        case 2000:
          sliderSpeed = 1000;
          break;
        case 1000:
          sliderSpeed = 900;
          break;
        case 900:
          sliderSpeed = 800;
          break;
        case 800:
          sliderSpeed = 700;
          break;

        default:
          sliderSpeed = 400;
      }

      print(sliderSpeed);
    } else if (sliderAnimation.value < 140 || sliderAnimation.value > 180) {
      monkeyTween.end = 20;
      monkeyController.forward();
      refreshPositionBanana = 0;
    }
  }

  @override
  void initState() {
    super.initState();

    monkeyController = AnimationController(
      duration: Duration(milliseconds: jumpMonkeySpeed),
      vsync: this,
      reverseDuration: Duration(seconds: 1),
    );
    final curve =
        CurvedAnimation(parent: monkeyController, curve: Curves.bounceIn);

    sliderController = AnimationController(
      duration: Duration(milliseconds: sliderSpeed),
      vsync: this,
    );

    monkeyTween = Tween(begin: 0.0, end: 80);
    monkeyAnimation = monkeyTween.animate(curve);

    monkeyAnimation.addListener(() {
      setState(() {});
      if (monkeyAnimation.isCompleted) {
        monkeyController.reverse();
      } else if (monkeyAnimation.isDismissed) {
        if (refreshLevel > 10) {
          monkeyTween.end = 100;
          positionBanana = 110;
          sliderSpeed = 10000;
          visibleText = !visibleText;
          refreshLevel = 0;
          highJump = 0;
        }
        visibleBanana = true;
        positionBanana += refreshPositionBanana;
        visibleText = false;
        level = refreshLevel;
      }
    });

    sliderTween = Tween(begin: 0.0, end: 320.0);
    sliderAnimation = sliderTween.animate(sliderController);
    sliderAnimation.addListener(() {
      setState(() {});
      if (sliderAnimation.isCompleted) {
        sliderController.duration = Duration(milliseconds: sliderSpeed);
        sliderController.reverse();
      } else if (sliderAnimation.isDismissed) {
        sliderController.duration = Duration(milliseconds: sliderSpeed);
        sliderController.forward();
      }
    });

    sliderController.forward();
  }

  @override
  void dispose() {
    monkeyController.dispose();
    sliderController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.blue,
          image: DecorationImage(
            image: AssetImage('images/Jungle.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Stack(
              overflow: Overflow.visible,
              alignment: Alignment.bottomCenter,
              children: [
                Positioned(
                  bottom: 400,
                  child: Text(
                    'LEVEL $level',
                    style: TextStyle(
                        fontSize: 50,
                        fontWeight: FontWeight.bold,
                        color:
                            visibleText ? Colors.transparent : Colors.yellow),
                  ),
                ),
                // Positioned(
                //   bottom: 300,
                //   child: Text(
                //     'GAME OVER',
                //     style: TextStyle(
                //         fontSize: 50,
                //         fontWeight: FontWeight.bold,
                //         color:
                //             visibleText ? Colors.orange : Colors.transparent),
                //   ),
                // ),
                Positioned(
                  // bottom: 150,
                  child: AnimatedOpacity(
                    opacity: visibleBanana ? 1 : 0,
                    duration: Duration(milliseconds: 1000),
                    child: Container(
                      padding: EdgeInsets.only(bottom: positionBanana),
                      child: Image(
                        height: 30.0,
                        width: 30.0,
                        image: AssetImage('images/banana.png'),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -30,
                  child: Container(
                    padding: EdgeInsets.only(bottom: monkeyAnimation.value),
                    child: Image(
                      height: 100.0,
                      width: 100.0,
                      image: AssetImage('images/monkey.png'),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(bottom: 30),
              child: FlutterSlider(
                values: [sliderAnimation.value],
                handlerWidth: 0,
                handlerHeight: 0,
                max: 360,
                min: 0,
                disabled: false,
                tooltip: FlutterSliderTooltip(
                  disabled: true,
                ),
                trackBar: FlutterSliderTrackBar(
                  activeTrackBar: BoxDecoration(
                    color: Colors.transparent,
                  ),
                  inactiveTrackBar: BoxDecoration(
                    color: Colors.transparent,
                  ),
                ),
                handler: FlutterSliderHandler(
                  decoration: BoxDecoration(),
                  child: Icon(
                    Icons.arrow_drop_down,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
                onDragging: (handlerIndex, lowerValue, newValue) {
                  sliderController.value = newValue;
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 6, right: 6),
              child: Container(
                height: 10.0,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    Colors.red,
                    Colors.yellow,
                    Colors.yellow,
                    Colors.yellow,
                    Colors.green,
                    Colors.yellow,
                    Colors.yellow,
                    Colors.yellow,
                    Colors.red,
                  ]),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.only(
                left: 5,
                right: 5,
              ),
              child: FlatButton(
                color: Colors.blue,
                child: Text(
                  'JUMP',
                  style: TextStyle(fontSize: 20.0, color: Colors.white),
                ),
                onPressed: () {
                  jumpMonkey();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
