import 'package:flutter/material.dart';
import 'package:jumping_monkey/start_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Monkey Jumping',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        // inputDecorationTheme:
      ),
      home: StartPage(),
    );
  }
}
